import datetime
from dateutil.relativedelta import relativedelta


# Выходные
holidays = (
    # '2020'
    datetime.date(day=1, month=1, year=2020),
    datetime.date(day=2, month=1, year=2020),
    datetime.date(day=3, month=1, year=2020),
    datetime.date(day=4, month=1, year=2020),
    datetime.date(day=5, month=1, year=2020),
    datetime.date(day=6, month=1, year=2020),
    datetime.date(day=7, month=1, year=2020),
    datetime.date(day=8, month=1, year=2020),
    datetime.date(day=22, month=2, year=2020),
    datetime.date(day=23, month=2, year=2020),
    datetime.date(day=24, month=2, year=2020),
    datetime.date(day=7, month=3, year=2020),
    datetime.date(day=8, month=3, year=2020),
    datetime.date(day=9, month=3, year=2020),
    datetime.date(day=1, month=5, year=2020),
    datetime.date(day=2, month=5, year=2020),
    datetime.date(day=3, month=5, year=2020),
    datetime.date(day=4, month=5, year=2020),
    datetime.date(day=5, month=5, year=2020),
    datetime.date(day=9, month=5, year=2020),
    datetime.date(day=10, month=5, year=2020),
    datetime.date(day=11, month=5, year=2020),
    datetime.date(day=12, month=6, year=2020),
    datetime.date(day=13, month=6, year=2020),
    datetime.date(day=14, month=6, year=2020),
    datetime.date(day=4, month=11, year=2020),
    # '2021'
    datetime.date(day=1, month=1, year=2021),
    datetime.date(day=2, month=1, year=2021),
    datetime.date(day=3, month=1, year=2021),
    datetime.date(day=4, month=1, year=2021),
    datetime.date(day=5, month=1, year=2021),
    datetime.date(day=6, month=1, year=2021),
    datetime.date(day=7, month=1, year=2021),
    datetime.date(day=8, month=1, year=2021),
    datetime.date(day=9, month=1, year=2021),
    datetime.date(day=10, month=1, year=2021),
    datetime.date(day=23, month=2, year=2021),
    datetime.date(day=8, month=3, year=2021),
    datetime.date(day=1, month=5, year=2021),
    datetime.date(day=2, month=5, year=2021),
    datetime.date(day=3, month=5, year=2021),
    datetime.date(day=8, month=5, year=2021),
    datetime.date(day=9, month=5, year=2021),
    datetime.date(day=10, month=5, year=2021),
    datetime.date(day=14, month=6, year=2021),
    datetime.date(day=4, month=11, year=2021)
)


# Проверка на выходные
def check_holidays(date):
    while (date in holidays):
        date = date + datetime.timedelta(1)
    return date
##################################################################################


# Дата решения суда
def Date_of_judgment():
    judgment_date = input('Введите дату решения суда: ')
    if judgment_date:
        judgment_date = judgment_date.split(".") # Разделили ввод на ДД ММ ГГГГ
        judgment_date = [int(item) for item in judgment_date] # Перевели в int
        judgment_date = datetime.date(day=judgment_date[0], month=judgment_date[1], year=judgment_date[2])
    return judgment_date
    
# judgment_date = datetime.date(day=1, month=1, year=2020)
##################################################################################

# Вступление в силу
def Entry_of_judgment(judgment_date):
    if appeal_verdict_date:
        entry_judgment = appeal_verdict_date
    else:
        entry_judgment = judgment_date + relativedelta(months=+1, days=+1)
        if entry_judgment.isoweekday() == 6:
            entry_judgment = entry_judgment + relativedelta(days=+2)
        elif entry_judgment.isoweekday() == 7:
            entry_judgment = entry_judgment + relativedelta(days=+1)
        entry_judgment = check_holidays(entry_judgment)
    return entry_judgment

# entry_judgment = judgment_date + relativedelta(months=+1, days=+1)
###################################################################################

# Дата до которой возможно подать апелляцию
def appeal_claim(judgment_date):
    date_for_appeal = judgment_date + relativedelta(months=+1)
    if date_for_appeal.isoweekday() == 6:
        date_for_appeal = date_for_appeal + relativedelta(days=+2)
    elif date_for_appeal.isoweekday() == 7:
        date_for_appeal = date_for_appeal + relativedelta(days=+1)
    date_for_appeal = check_holidays(date_for_appeal)
    return date_for_appeal

# date_for_appeal = judgment_date + relativedelta(months=+1)
###################################################################################

# Дата подача аппеляции
def date_of_appeal():
    appeal_date = input('Введите дату подачи апелляции: ')
    if appeal_date:
        appeal_date = appeal_date.split(".") # Разделили ввод на ДД ММ ГГГГ
        appeal_date = [int(item) for item in appeal_date] # Перевели в int
        appeal_date = datetime.date(day=appeal_date[0], month=appeal_date[1], year=appeal_date[2])
    else:
        appeal_date = None
    return appeal_date

# appeal_date = datetime.date(day=10, month=1, year=2020)
# appeal_date = None
###################################################################################

# Решение аппеляции
def appeal_verdict(appeal_date):
    appeal_date = appeal_date
    if appeal_date:
        appeal_verdict_date = input('Введите дату решения апелляции: ')
        if appeal_verdict_date:
            appeal_verdict_date = appeal_verdict_date.split(".") # Разделили ввод на ДД ММ ГГГГ
            appeal_verdict_date = [int(item) for item in appeal_verdict_date] # Перевели в int
            appeal_verdict_date = datetime.date(day=appeal_verdict_date[0], month=appeal_verdict_date[1], year=appeal_verdict_date[2])
    else:
        appeal_verdict_date = None
    return appeal_verdict_date

# appeal_verdict_date = datetime.date(day=20, month=1, year=2020)
# appeal_verdict_date = None
###################################################################################


# Подача кассации до
def cassation_date():
    date_for_сassation = entry_judgment + relativedelta(months=+3)
    if date_for_сassation.isoweekday() == 6:
        date_for_сassation = date_for_сassation + relativedelta(days=+2)
    elif date_for_сassation.isoweekday() == 7:
        date_for_сassation = date_for_сassation + relativedelta(days=+1)
    date_for_сassation = check_holidays(date_for_сassation)
    return date_for_сassation

###################################################################################

# кассация подана
def start_cassation():
    date_start_cassation = input('Введите дату подачи кассации: ')
    if date_start_cassation:
        date_start_cassation = date_start_cassation.split(".") # Разделили ввод на ДД ММ ГГГГ
        date_start_cassation = [int(item) for item in date_start_cassation] # Перевели в int
        date_start_cassation = datetime.date(day=date_start_cassation[0], month=date_start_cassation[1], year=date_start_cassation[2])
    else:
        date_start_cassation = None
    return date_start_cassation
# date_start_cassation = datetime.date(day=27, month=1, year=2020)
# date_start_cassation = None
###################################################################################


# Решения по кассации
def cassation_verdict():
    date_cassation_verdict = input('Введите дату решения кассации: ')
    if date_cassation_verdict:
        date_cassation_verdict = date_cassation_verdict.split(".") # Разделили ввод на ДД ММ ГГГГ
        date_cassation_verdict = [int(item) for item in date_cassation_verdict] # Перевели в int
        date_cassation_verdict = datetime.date(day=date_cassation_verdict[0], month=date_cassation_verdict[1], year=date_cassation_verdict[2])
    else:
        date_cassation_verdict = None
    return date_cassation_verdict

# date_cassation_verdict = datetime.date(day=10, month=2, year=2020)
# date_cassation_verdict = None
###################################################################################

# Обжалование в верховном суде
def tribunal_date():
    date_for_tribunal = date_cassation_verdict + relativedelta(months=+3)
    if date_for_tribunal.isoweekday() == 6:
        date_for_tribunal = date_for_tribunal + relativedelta(days=+2)
    elif date_for_tribunal.isoweekday() == 7:
        date_for_tribunal = date_for_tribunal + relativedelta(days=+1)
    date_for_tribunal = check_holidays(date_for_tribunal)
    return date_for_tribunal
###################################################################################


def Count_work_days(startdate, days):
    fromdate = startdate
    x = 1
    while x < days:
        a = fromdate.isoweekday()
        if a != 6 and a != 7:
            x = x + 1
            fromdate = fromdate + datetime.timedelta(1)
        else:
            fromdate = fromdate + datetime.timedelta(1)
    return fromdate



def Print_NoDate():
    #'Работает без корректировки решений обжалований'
    print('Дата решения суда \t {}'.format(judgment_date.strftime('%d.%m.%Y')))
    print('Подача апелляция до \t {}'.format(date_for_appeal.strftime('%d.%m.%Y')))
    print('Вступление в силу \t {}'.format(entry_judgment.strftime('%d.%m.%Y')))
    print('Подача кассации до \t {}'.format(date_for_сassation.strftime('%d.%m.%Y')))
    print('В верховный суд до \t {}'.format(date_for_tribunal.strftime('%d.%m.%Y')))


def Cassacion():
    #'Работает с переменными дата апелляции и дата решения по апелляции'
    print('Дата решения суда \t {}'.format(judgment_date.strftime('%d.%m.%Y')))
    print('Дата вступление в силу \t {}'.format(entry_judgment.strftime('%d.%m.%Y')))
    if appeal_date:    
        print('Апелляция подана \t {}'.format(appeal_date.strftime('%d.%m.%Y')))
        if not appeal_verdict_date:
            print('Решение по апелляции \t {}'.format('Апелляция на рассмотрении'))
    else:
        print('Подача апелляция до \t {}'.format(date_for_appeal.strftime('%d.%m.%Y')))
        print('Решение по апелляции \t {}'.format('Ожидается вышестоящие решение'))
    if appeal_verdict_date:
        print('Решение по апелляции \t {}'.format(appeal_verdict_date.strftime('%d.%m.%Y')))
        if date_start_cassation:
            print('Дата подачи кассации \t {}'.format(date_start_cassation.strftime('%d.%m.%Y')))
            if date_cassation_verdict:
                print('Решение по кассации \t {}'.format(date_cassation_verdict.strftime('%d.%m.%Y')))
                print('В верховный суд до \t {}'.format(date_for_tribunal.strftime('%d.%m.%Y')))
            else:
                print('Решение по кассации \t {}'.format('Кассация на рассмотрении'))
                print('В верховный суд до \t {}'.format('Ожидается вышестоящие решение'))
        else:
            print('Подача кассации до \t {}'.format(date_for_сassation.strftime('%d.%m.%Y')))
            print('Решение по кассации \t {}'.format('Ожидается вышестоящие решение'))
            print('В верховный суд до \t {}'.format('Ожидается вышестоящие решение'))
    else:
        print('Подача кассации до \t {}'.format('Ожидается вышестоящие решение'))
        print('Решение по кассации \t {}'.format('Ожидается вышестоящие решение'))
        print('В верховный суд до \t {}'.format('Ожидается вышестоящие решение'))

print('------------------------------------------------------')

try:
    judgment_date = Date_of_judgment()
    date_for_appeal = appeal_claim(judgment_date)
    appeal_date = date_of_appeal()
    if appeal_date:
        appeal_verdict_date = appeal_verdict(appeal_date)
    else:
        appeal_verdict_date = None
    entry_judgment = Entry_of_judgment(judgment_date)
    if appeal_verdict_date:
        date_for_сassation = cassation_date()
    else:
        date_for_сassation = None
    if appeal_verdict_date:
        date_start_cassation = start_cassation()
    else:
        date_start_cassation = None
    if date_start_cassation:
        date_cassation_verdict = cassation_verdict()
    else:
        date_cassation_verdict = None
    if date_cassation_verdict:
        date_for_tribunal = tribunal_date()
    else:
        date_for_tribunal = None


    print('------------------------------')
    Cassacion()
except:
    print('Проверьте правильность ввода даты - образец: 01.01.2020')
finally:
    close_window = input('Нажмите ENTER чтобы выйти')
