from PyQt5 import QtCore, QtGui, QtWidgets
from dateutil.relativedelta import relativedelta
import datetime
import ui


holidays = (
    # '2020'
    datetime.date(day=1, month=1, year=2020),
    datetime.date(day=2, month=1, year=2020),
    datetime.date(day=3, month=1, year=2020),
    datetime.date(day=4, month=1, year=2020),
    datetime.date(day=5, month=1, year=2020),
    datetime.date(day=6, month=1, year=2020),
    datetime.date(day=7, month=1, year=2020),
    datetime.date(day=8, month=1, year=2020),
    datetime.date(day=22, month=2, year=2020),
    datetime.date(day=23, month=2, year=2020),
    datetime.date(day=24, month=2, year=2020),
    datetime.date(day=7, month=3, year=2020),
    datetime.date(day=8, month=3, year=2020),
    datetime.date(day=9, month=3, year=2020),
    datetime.date(day=1, month=5, year=2020),
    datetime.date(day=2, month=5, year=2020),
    datetime.date(day=3, month=5, year=2020),
    datetime.date(day=4, month=5, year=2020),
    datetime.date(day=5, month=5, year=2020),
    datetime.date(day=9, month=5, year=2020),
    datetime.date(day=10, month=5, year=2020),
    datetime.date(day=11, month=5, year=2020),
    datetime.date(day=12, month=6, year=2020),
    datetime.date(day=13, month=6, year=2020),
    datetime.date(day=14, month=6, year=2020),
    datetime.date(day=4, month=11, year=2020),
    # '2021'
    datetime.date(day=1, month=1, year=2021),
    datetime.date(day=2, month=1, year=2021),
    datetime.date(day=3, month=1, year=2021),
    datetime.date(day=4, month=1, year=2021),
    datetime.date(day=5, month=1, year=2021),
    datetime.date(day=6, month=1, year=2021),
    datetime.date(day=7, month=1, year=2021),
    datetime.date(day=8, month=1, year=2021),
    datetime.date(day=9, month=1, year=2021),
    datetime.date(day=10, month=1, year=2021),
    datetime.date(day=23, month=2, year=2021),
    datetime.date(day=8, month=3, year=2021),
    datetime.date(day=1, month=5, year=2021),
    datetime.date(day=2, month=5, year=2021),
    datetime.date(day=3, month=5, year=2021),
    datetime.date(day=8, month=5, year=2021),
    datetime.date(day=9, month=5, year=2021),
    datetime.date(day=10, month=5, year=2021),
    datetime.date(day=14, month=6, year=2021),
    datetime.date(day=4, month=11, year=2021)
)

def check_holidays(date):
    """Checking the date for holiday, while the date in the list 'holiday' the date  = +1 day. And then run checking for weekend. """
    while (date in holidays):
        date = date + datetime.timedelta(1)
    date = check_weekend(date)
    return date


def check_weekend(date):
    """Checking the date for weekend, if Saturday then date +2 days, if Sunday then date +1 day"""
    if date.isoweekday() == 6:
        date = date + relativedelta(days=+2)
    elif date.isoweekday() == 7:
        date = date + relativedelta(days=+1)
    return date


class MyApp(QtWidgets.QMainWindow, ui.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.checkBox_2.stateChanged.connect(self.switch_checkbox_2)
        self.checkBox_3.stateChanged.connect(self.switch_checkbox_3)
        self.checkBox_4.stateChanged.connect(self.switch_checkbox_4)
        self.checkBox_5.stateChanged.connect(self.switch_checkbox_5)
        self.pushButton.clicked.connect(self.Entry_of_judgment)
        self.pushButton.clicked.connect(self.appeal_claim)
        self.pushButton.clicked.connect(self.cassation_date)
        self.pushButton.clicked.connect(self.tribunal_date)


    def Entry_of_judgment(self):
        """If there is a decision of the court of appeal, then the entry into force is the date of the decision of the appeal. Else then entry into force is date of district court + 1 day and 1 month"""
        if self.checkBox_3.isChecked():
            temp_date = self.dateEdit_3.date() # parsing date
            var_date = temp_date.toPyDate() # convert to datetime
            self.label_1_1.setText(var_date.strftime('%d.%m.%Y'))
        else:
            temp_date = self.dateEdit_1.date()
            temp_date = temp_date.toPyDate() # convert to datetime
            temp_date = temp_date + relativedelta(months=+1, days=+1)
            var_date = check_holidays(temp_date)
            self.label_1_1.setText(var_date.strftime('%d.%m.%Y'))


    def appeal_claim(self):
        """Deadline for filing an appeal is one month from the date of the judgment by the district court"""
        temp_date = self.dateEdit_1.date()
        temp_date = temp_date.toPyDate()
        temp_date = temp_date + relativedelta(months=+1)
        var_date = check_holidays(temp_date)
        self.label_2_2.setText(var_date.strftime('%d.%m.%Y'))


    def cassation_date(self):
        """Deadline a cassation appeal is 3 months from the date of entry into force of the court decision"""
        if self.checkBox_3.isChecked():
            temp_date = self.label_1_1.text() # parse text from label
            temp_date = temp_date.split('.') 
            temp_date = [int(item) for item in temp_date] # convert to int
            temp_date = datetime.date(day=temp_date[0], month=temp_date[1], year=temp_date[2])
            temp_date = temp_date + relativedelta(months=+3)
            var_date = check_holidays(temp_date)
            self.label_3_3.setText(var_date.strftime('%d.%m.%Y'))


    def tribunal_date(self):
        """If there is a decision of the cassation court, then from this date there are 3 months to appeal to the Supreme Court"""
        if self.checkBox_5.isChecked():
            temp_date = self.dateEdit_5.date()
            temp_date = temp_date.toPyDate()
            temp_date = temp_date + relativedelta(months=+3)
            var_date = check_holidays(temp_date)
            self.label_4_4.setText(var_date.strftime('%d.%m.%Y'))

    #Access to date picker
    def switch_checkbox_2(self):
        if self.checkBox_2.isChecked():
            self.dateEdit_2.setEnabled(True)
        else:
            self.dateEdit_2.setEnabled(False)

    def switch_checkbox_3(self):
        if self.checkBox_3.isChecked():
            self.dateEdit_3.setEnabled(True)
        else:
            self.dateEdit_3.setEnabled(False)

    def switch_checkbox_4(self):
        if self.checkBox_4.isChecked():
            self.dateEdit_4.setEnabled(True)
        else:
            self.dateEdit_4.setEnabled(False)

    def switch_checkbox_5(self):
        if self.checkBox_5.isChecked():
            self.dateEdit_5.setEnabled(True)
        else:
            self.dateEdit_5.setEnabled(False)


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = MyApp()
    window.show()
    app.exec_()
    